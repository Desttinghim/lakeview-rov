extern int Joyvar5;
extern int Joyvar6;
int Dpad;
const int Up = 16;
const int Down = 4;
int trigger;
int PWMServo = 150;
int PWMClaw = 500;
int PWMPan = 300;
int PWMTilt = 300;
int Camera;



void Arm(){
  Dpad = Joyvar5;
  Camera = Joyvar6;

  if (Dpad == 1){
    if(PWMClaw > 250){
      PWMClaw = PWMClaw-1;
      if(Dpad == 0){
        if(PWMClaw < 500){
          PWMClaw= PWMClaw+1;
          pwm.setPWM(8,0,PWMClaw);
          delay(25);
        }
      }
      pwm.setPWM(8,0,PWMClaw);
      delay(25);
    }
  }

  else if(Dpad == 2){
    if(PWMClaw < 500){
      PWMClaw= PWMClaw+10;
      pwm.setPWM(8,0,PWMClaw);
      delay(25);
    }
  }
  if (Dpad == 16){
    if(PWMServo > 150){
      PWMServo = PWMServo-10;
      pwm.setPWM(9,0,PWMServo);
      delay(25);
    }
  }
  else if(Dpad == 4){
    if(PWMServo < 600){
      PWMServo = PWMServo+10;
      pwm.setPWM(9,0,PWMServo);
      delay(25);
    }
  }
  if (Camera == 2){
    if(PWMTilt > 150){
      PWMTilt = PWMTilt-10;
      Serial.println(PWMTilt);

      pwm.setPWM(10,0,PWMTilt);
      delay(25);
    }
  }
  else if(Camera == 1){
    if(PWMTilt < 450){
      PWMTilt = PWMTilt+10;

      pwm.setPWM(10,0,PWMTilt);

      delay(25);
    }
  }
  if (Camera == 8){
    if(PWMPan > 350){
      PWMPan = PWMPan-10;
      pwm.setPWM(11,0,PWMPan);
      delay(25);
    }
  }
  else if(Camera == 4){
    if(PWMPan < 550){
      PWMPan = PWMPan+10;
      pwm.setPWM(11,0,PWMPan);
      delay(25);
    }
  }

  Serial.println(PWMPan);
}