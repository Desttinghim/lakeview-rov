
int PWMVal1; 
int FBVal;
int LRVal;
int TurnVal;

void Forward_and_Back() 
{
	extern int Joyvar1;
	extern int Joyvar0;
	extern int Joyvar3;
	TurnVal = map(Joyvar3, 0, 0xFF, -255, 255);
	FBVal = map(Joyvar1, 0x3FF, 0, -255, 255);
	LRVal = map(Joyvar0, 0, 0x3FF, -255, 255);\
	Serial.println(FBVal);  
	if (abs(FBVal) >= abs(LRVal)){

		if (FBVal > 20){ //Forwards
			if (TurnVal > 50){ //Turn Right
			    TurnRight();
				PWMVal1 = FBVal;
			} else if (TurnVal <-50){ // Turn Left
			    TurnLeft();
				PWMVal1 = map(FBVal,-255,0,0,255);
			} else{ // Forwards
			    Forward();
			}
		}
		else if (FBVal <-20){ // Backwards

			if (TurnVal >50){ // Turn RIGHT
			    TurnRight();
				PWMVal1 = TurnVal;}
			else if (TurnVal <-50){ // Turn Left
			    TurnLeft();
				PWMVal1 = map(TurnVal,-255,0,0,255);
			} else{ // 
			    Backward();
			}
		} else{ //Motors OFF
			digitalWrite(M1P1, LOW);
			digitalWrite(M1P2, LOW);
			digitalWrite(M2P1, LOW);
			digitalWrite(M2P2, LOW);
			digitalWrite(M3P1, LOW);
			digitalWrite(M3P2, LOW);
			digitalWrite(M4P1, LOW);
			digitalWrite(M4P2, LOW);
		}


	} else {
		if (LRVal > 20){ //Strafe Right
			StrafeRight();
			PWMVal1 = LRVal;
		} else if (LRVal <-20){ // Strafe Left
			StrafeLeft();
			PWMVal1 = map(LRVal,-255,0,255,0);
		} else{ //Motors OFF
			digitalWrite(M1P1, LOW);
			digitalWrite(M1P2, LOW);
			digitalWrite(M2P1, LOW);
			digitalWrite(M2P2, LOW);
			digitalWrite(M3P1, LOW);
			digitalWrite(M3P2, LOW);
			digitalWrite(M4P1, LOW);
			digitalWrite(M4P2, LOW);
		}
	}

	analogWrite(PWM1,PWMVal1);
	analogWrite(PWM2,PWMVal1);
	analogWrite(PWM3,PWMVal1);
	analogWrite(PWM4,PWMVal1);

}


void Forward(){
	digitalWrite(M1P1, HIGH);
	digitalWrite(M1P2, LOW);
	digitalWrite(M2P1, HIGH);
	digitalWrite(M2P2, LOW);
	digitalWrite(M3P1, LOW);
	digitalWrite(M3P2, HIGH);
	digitalWrite(M4P1, LOW);
	digitalWrite(M4P2, HIGH);
}

void Backward(){
	digitalWrite(M1P1, LOW);
	digitalWrite(M1P2, HIGH);
	digitalWrite(M2P1, LOW);
	digitalWrite(M2P2, HIGH);
	digitalWrite(M3P1, HIGH);
	digitalWrite(M3P2, LOW);
	digitalWrite(M4P1, HIGH);
	digitalWrite(M4P2, LOW);
}

void TurnRight(){
	digitalWrite(M1P1, LOW);
	digitalWrite(M1P2, HIGH);
	digitalWrite(M2P1, HIGH);
	digitalWrite(M2P2, LOW);
	digitalWrite(M3P1, LOW);
	digitalWrite(M3P2, HIGH);
	digitalWrite(M4P1, HIGH);
	digitalWrite(M4P2, LOW);
}

void TurnLeft(){
	digitalWrite(M1P1, HIGH);
	digitalWrite(M1P2, LOW);
	digitalWrite(M2P1, LOW);
	digitalWrite(M2P2, HIGH);
	digitalWrite(M3P1, HIGH);
	digitalWrite(M3P2, LOW);
	digitalWrite(M4P1, LOW);
	digitalWrite(M4P2, HIGH);
}

void StrafeLeft(){
	digitalWrite(M1P1, LOW);
	digitalWrite(M1P2, HIGH);
	digitalWrite(M2P1, LOW);
	digitalWrite(M2P2, HIGH);
	digitalWrite(M3P1, LOW);
	digitalWrite(M3P2, HIGH);
	digitalWrite(M4P1, LOW);
	digitalWrite(M4P2, HIGH);
}

void StrafeRight(){
	digitalWrite(M1P1, HIGH);
	digitalWrite(M1P2, LOW);
	digitalWrite(M2P1, HIGH);
	digitalWrite(M2P2, LOW);
	digitalWrite(M3P1, HIGH);
	digitalWrite(M3P2, LOW);
	digitalWrite(M4P1, HIGH);
	digitalWrite(M4P2, LOW);
}