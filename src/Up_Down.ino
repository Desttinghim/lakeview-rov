int PWMVal2; 
int UDVal;

void Up_Down() {
  extern int Joyvar4;
  extern int Joyvar5;
  UDVal = map(Joyvar4, 0xFF, 0, -255, 255);

  if (UDVal <= -40){    
    PWMVal2 = map(UDVal, -255, 0, 255, 0);

    digitalWrite(M5P1, LOW);
    digitalWrite(M5P2, HIGH);
    digitalWrite(M6P1, HIGH);
    digitalWrite(M6P2, LOW);
    digitalWrite(M7P1, LOW);
    digitalWrite(M7P2, HIGH);
    digitalWrite(M8P1, HIGH);
    digitalWrite(M8P2, LOW);
  } else if (UDVal >= 40) {
    digitalWrite(M5P1, HIGH);
    digitalWrite(M5P2, LOW);
    digitalWrite(M6P1, LOW);
    digitalWrite(M6P2, HIGH);
    digitalWrite(M7P1, HIGH);
    digitalWrite(M7P2, LOW);
    digitalWrite(M8P1, LOW);
    digitalWrite(M8P2, HIGH);
    delay(10);
    PWMVal2 = map(UDVal, 0, 255, 0, 255);
  } else {
    digitalWrite(M5P1, LOW);
    digitalWrite(M5P2, LOW);
    digitalWrite(M6P1, LOW);
    digitalWrite(M6P2, LOW);
    digitalWrite(M7P1, LOW);
    digitalWrite(M7P2, LOW);
    digitalWrite(M8P1, LOW);
    digitalWrite(M8P2, LOW);
  }

  analogWrite(PWM5, PWMVal2);
  analogWrite(PWM6, PWMVal2);
  analogWrite(PWM7, PWMVal2);
  analogWrite(PWM8, PWMVal2);
}
