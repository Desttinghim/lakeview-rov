/* Simplified Logitech Extreme 3D Pro Joystick Report Parser */

#include <Arduino.h>
#include <Wire.h>
#include <Adafruit_PWMServoDriver.h>
#include <hid.h>
#include <hiduniversal.h>
#include <usbhub.h>

#include "le3dp_rptparser.h"

USB                                             Usb;
USBHub                                          Hub(&Usb);
HIDUniversal                                    Hid(&Usb);
JoystickEvents                                  JoyEvents;
JoystickReportParser                            Joy(&JoyEvents);
Adafruit_PWMServoDriver pwm = Adafruit_PWMServoDriver();

#define SERVOMIN  150 // this is the 'minimum' pulse length count (out of 4096)
#define SERVOMAX  500 

const int PWM1 = 2;
const int PWM2 = 5;
const int PWM3 = 4;
const int PWM4 = 3;
const int PWM5 = 6;
const int PWM6 = 7;
const int PWM7 = 8;
const int PWM8 = 9;

const int M1P1 = 22;
const int M1P2 = 23;
const int M2P1 = 24;
const int M2P2 = 25;
const int M3P1 = 26;
const int M3P2 = 27;
const int M4P1 = 29;
const int M4P2 = 30;

const int M5P1 = 44;
const int M5P2 = 45;
const int M6P1 = 48;
const int M6P2 = 49;
const int M7P1 = 50;
const int M7P2 = 51;
const int M8P1 = 46;
const int M8P2 = 47;

uint8_t servonum = 0;



void setup()
{
	Serial.begin( 9600 );
	while (!Serial); // Wait for serial port to connect - used on Leonardo, Teensy and other boards with built-in USB CDC serial connection
	Serial.println("Start");

	if (Usb.Init() == -1) {
		Serial.println("OSC did not start.");
	}

	delay( 200 );

	if (!Hid.SetReportParser(0, &Joy)) {
		ErrorMessage<uint8_t>(PSTR("SetReportParser"), 1  );
	}
   

	pinMode(PWM1, OUTPUT);  
	pinMode(PWM2, OUTPUT);
	pinMode(PWM3, OUTPUT);  
	pinMode(PWM4, OUTPUT);
	pinMode(PWM5, OUTPUT);  
	pinMode(PWM6, OUTPUT);
	pinMode(PWM7, OUTPUT);  
	pinMode(PWM8, OUTPUT);
	pinMode(M1P1, OUTPUT);  
	pinMode(M1P2, OUTPUT);
	pinMode(M2P1, OUTPUT);  
	pinMode(M2P2, OUTPUT);
	pinMode(M3P1, OUTPUT);  
	pinMode(M3P2, OUTPUT);
	pinMode(M4P1, OUTPUT);  
	pinMode(M4P2, OUTPUT);
	pinMode(M5P1, OUTPUT);  
	pinMode(M5P2, OUTPUT);
	pinMode(M6P1, OUTPUT);  
	pinMode(M6P2, OUTPUT);
	pinMode(M7P1, OUTPUT);  
	pinMode(M7P2, OUTPUT);
	pinMode(M8P1, OUTPUT);  
	pinMode(M8P2, OUTPUT);

	pwm.begin();

	pwm.setPWMFreq(60);
}

void setServoPulse(uint8_t n, double pulse) {
  double pulselength;
  
  pulselength = 1000000;   // 1,000,000 us per second
  pulselength /= 60;   // 60 Hz
  Serial.print(pulselength); Serial.println(" us per period"); 
  pulselength /= 4096;  // 12 bits of resolution
  Serial.print(pulselength); Serial.println(" us per bit"); 
  pulse *= 1000;
  pulse /= pulselength;
  Serial.println(pulse);
  pwm.setPWM(n, 0, pulse);
}

void loop() {
    Usb.Task();
    Arm();
    Forward_and_Back();
    Up_Down();
}


// 
int main(void) {
  init();

#if defined(USBCON)
  USB.attach();
#endif

  setup();

  while(true) {
    loop();
    if (serialEventRun) serialEventRun();
  }

  return 0;
}